package qa.test

import clients.AwsClient
import clients.ECDynamoDAO
import clients.ETDynamoDAO
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.util.IOUtils
import com.google.gson.GsonBuilder
import org.jsoup.Jsoup
import org.testng.annotations.Test
import java.io.File
import java.security.MessageDigest

class GetMetaOfTranscriptsFileTest {
    private val gson = GsonBuilder().create()

    @Test(groups = ["get-transcripts-metadata"])
    fun getS3Files(){
        val docId = System.getProperty("test.text").trim()
        var message = ""
        if (docId.startsWith("TTR")){
            val bucketName = "as.archive.feeds.transcripts.tr"
            val prefix = "Transcripts/${docId.split("_")[1]}"
            val data = mutableListOf(" fileName             | last modified                | size(KB) \n")

            try {
                val objectSummaries = AwsClient.s3Client.listObjects(ListObjectsRequest(bucketName, prefix, "", ",", 1000)).objectSummaries
                objectSummaries.forEach {
                    data.add("${it.key.replace("Transcripts/", "")} | ${it.lastModified} | ${String.format("%.02f", it.size / 1024F)}\n")
                }
                if (objectSummaries.isNotEmpty()){
                    message = "Please find requested data for $docId \n```${data.toString().replace("[", "").replace("]", "").replace(",", "")}```"
                }
                else
                    message = "Please check docId. No versions found. :white_frowning_face:"

            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                // To ensure that the network connection doesn't remain open, close any open input streams.
            }
        }
        else if (docId.startsWith("ET")){
            val headers = "\n" + String.format("%21s", "CopyType |") + String.format("%26s", "Creation Time |") + String.format("%-50s", " File Name") + "\n"
            val data = mutableListOf(headers)
            try {
                val etItem = ETDynamoDAO.getCopyDetailsOfEnhancedTranscript(docId)

                etItem.copyDetails.forEach { copyDetails ->
                    copyDetails.copyDetails.forEach { copyDetails2 ->
                        val recordData = String.format("%20s", "${copyDetails.copyType} |") + String.format("%26s", " ${copyDetails2.creationTime} |") + String.format("%-50s", " ${copyDetails2.fileName}") + "\n"
                        data.add(recordData)
                    }
                }

                if (etItem == null){
                    message = "Please check docId. No versions found. :white_frowning_face:"
                } else
                    message = "Please find requested data for $docId \n```${data.toString().replace("[", "").replace("]", "").replace(",", "")}```"
            }
            catch (e: java.lang.Exception){
                e.printStackTrace()
            }
        }
        else if (docId.startsWith("PR")){
            val hashedDocumentId = "${getMd5HashOfString(docId).substring(0, 5)}.$docId"
            val downloaderBucket = "as.docstorage.downloaded"
            try {
                val rdfFile = AwsClient.s3Client.getObject(downloaderBucket, "$hashedDocumentId/Downloaded/${docId}.rdf")?.objectContent
                val s3Url = "https://s3.console.aws.amazon.com/s3/object/as.docstorage.downloaded?region=us-east-1&prefix=$hashedDocumentId/Downloaded/$docId.rdf";
                val rdfString = IOUtils.toString(rdfFile)
                val parsedDoc = Jsoup.parse(rdfString)
                val providerCode = parsedDoc.getElementsByTag("xn:vendorData")
                        .first { it.text().startsWith("AMX:Special Code=PS/p.") }.text().replace("AMX:Special Code=PS/p.", "")
                val serviceCode = parsedDoc.getElementsByTag("xn:vendorData")
                        .first { it.text().startsWith("AMX:Special Code=PS/s.") }.text().replace("AMX:Special Code=PS/s.", "")
                val sourceCode = parsedDoc.getElementsByTag("xn:vendorData").first(){it.text().startsWith("NEWSEDGE:Special Code=PS/.")}
                        .text().replace("NEWSEDGE:Special Code=PS/.", "")

                message = "Please find requested data for $docId \n Provider Code :- ${providerCode.trimEnd('_')}" +
                        "\n Service Code :- ${serviceCode.trimEnd('_')}" +
                        "\n Source Code :- ${sourceCode.trimEnd('_')}" +
                        "\n For more information click on :- <$s3Url|RDF FIlE S3 LINK>"
            } catch (ae : AmazonS3Exception){
                message = "Exception occurred. Please check entered document id $docId. :white_frowning_face:"
            }
        } else if (docId.startsWith("EC-")){
            try {
                val allMeta = ECDynamoDAO.getLatestVersionMeta(docId).sortedBy { it.creationDatetime }.first()
                message = """Please find requested meta for *$docId* ```
Id:                ${allMeta.id}
Flag:              ${allMeta.flag}
Perspective:       ${allMeta.perspective}
CallDate:          ${allMeta.callDate}
PublishedAt:       ${allMeta.publishedAt}
Audio:             ${if (allMeta.audio.isNotEmpty()) "Available" else "Not Available"}
Summary:           ${if (allMeta.summary?.isNullOrBlank() == false) "Available" else "Not Available"}
ParentVendor:      ${allMeta.parentVendor}
RelevantRole:      ${allMeta.relevantRole}
Primary Ticker:    ${gson.toJson(allMeta.tickers.filter { it.isPrimary })}
Secondary Ticker:  ${gson.toJson(allMeta.tickers.filter { !it.isPrimary })}```""".trimIndent()
            } catch (e: Exception){
                e.printStackTrace()
                message = "Exception occurred. Please check entered document id $docId. :white_frowning_face:"
            }
        }
        println(message)
        File(System.getProperty("user.dir") + "/temp.txt")
                .writeText(message)
    }

    private fun getMd5HashOfString(text: String): String {
        val md = MessageDigest.getInstance("MD5")
        val md5HashBytes = md.digest(text.toByteArray()).toTypedArray()
        val md5OfDocId = StringBuilder(md5HashBytes.size * 2)
        md.digest(text.toByteArray()).forEach { byte ->
            md5OfDocId.append(String.format("%2X", byte).replace(" ", "0"))
        }
        return md5OfDocId.toString().toLowerCase()
    }
}