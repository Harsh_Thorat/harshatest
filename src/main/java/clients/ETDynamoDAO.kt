package clients

import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.PrimaryKey
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec
import com.google.gson.Gson

object ETDynamoDAO {
    val tableName = "prod_feeds_enhanced_tr_downloads"
    val gson = Gson()

    fun getCopyDetailsOfEnhancedTranscript(docId: String): EnhancedFeedTrItem{
        val requestedItem = GetItemSpec().withPrimaryKey(PrimaryKey("id", docId))
        val item = DynamoDB(AwsClient.dynamoClient).getTable(tableName).getItem(requestedItem)
        return gson.fromJson(item.toJSON(), EnhancedFeedTrItem::class.java)
    }

    data class EnhancedFeedTrItem(
            val id: String,
            val docDatetime: String,
            val updateDatetime: String,
            val copyDetails: List<CopyDetails>
    ){
        data class CopyDetails(
                val copyDetails: List<CopyDetails2>,
                val copyType: String
        ){
            data class CopyDetails2(
                    val fileName: String,
                    val creationTime: String
            )
        }
    }
}