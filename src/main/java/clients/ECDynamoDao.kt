package clients

import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap
import com.google.gson.Gson

object ECDynamoDAO {
    val tableName = "downloader_feeds_expert_calls_tr_documents"
    val gson = Gson()

    fun getLatestVersionMeta(docId: String): List<ExpertCallMetadata>{
        val requestedSpec =
            QuerySpec().withKeyConditionExpression("id = :val1").withValueMap(ValueMap().withString(":val1", docId))
        val items = DynamoDB(AwsClient.dynamoClient).getTable(tableName).query(requestedSpec)
        return items.map {
            gson.fromJson(it?.toJSON(), ExpertCallMetadata::class.java)
        }
    }

    data class ExpertCallMetadata(
        val id: String,
        val creationDatetime: String,
        val lastUpdatedAt: String,
        val flag: String,
        val perspective: String,
        val relevantRole: String,
        val publishedAt: String,
        val audio: String,
        val callDate: String,
        val tickers: List<Ticker>,
        val summary: String?,
        val parentVendor: String?
    ){
        data class Ticker(
            val ticker: Ticker2,
            val isPrimary: Boolean
        ){
            data class Ticker2(
                val company: Company
            ){
                data class Company(
                    val cik: String,
                    val title: String,
                    val tickers: List<Company>
                )
            }
        }
    }
}